const express = require('express');
const app = express();
const expressWs = require('express-ws')(app);
const crypto = require('crypto');

/**
 *
 * @type {string}
 */
const algorithm = 'aes-256-ctr';

/**
 *
 * @type {string}
 */
const password = '7v411hmhNRlJ';


/**
 *
 * @param text
 * @returns {void | Promise<void> | * | IDBRequest<IDBValidKey>}
 */
const encrypt = function(text){
  let cipher = crypto.createCipher(algorithm,password);
  let crypted = cipher.update(text,'utf8','hex');
  crypted += cipher.final('hex');
  return crypted;
};

/**
 *
 * @type {boolean}
 */
let running = false;

/**
 * Middleware
 */
app.use(function (req, res, next) {
  return next();
});


/**
 *
 * @param min
 * @param max
 * @returns {*}
 */
function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}


/**
 *  Router
 */
app.ws('/', function(ws, req) {

  /**
   *
   */
  const sendData = function(){

    if(!running) return false;
    
    /**
     * Send Message
     */
    ws.send(encrypt(JSON.stringify({
      id: Math.random(),
      type: "update",
      message: Math.random().toString(36),
      data: Math.random().toString(36),
    })));

    /**
     * Run in random interval
     */
    setTimeout(function(){
      sendData();
    }, getRandom(1000, 4000))
  };

  if(!running){
    running = true;
    sendData();
  }

  /**
   * Only for debug
   */
  ws.on('message', function(msg) {
    console.log(msg);
  });


});

/**
 * PORT
 */
app.listen(5501);
